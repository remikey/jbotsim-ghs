[![License](https://img.shields.io/badge/license-LGPL%20&ge;%203.0-informational.svg)][lgpl3]

# GHS algorithm on JBotSim




* [Introduction](#introduction)
* [Running the GHS algorithm with JBotSim](#running-the-ghs-algorithm-with-jbotsim)
    + [How to run it](#how-to-run-it)
    + [Color codes and states](#color-codes-and-states)
    + [Parameters](#parameters)
* [Details on the GHS algorithm](#details-on-the-ghs-algorithm-and-our-implementation)
    + [Principle and complexity](#principle-and-complexity)
    + [Details operation](#detailed-operation)
* [License](#license)


## Introduction


This project uses the [JBotSim library][jbotsim-web] to implement the [GHS algorithm][GHS-wiki], which computes
a [minimum spanning tree (MST)](https://en.wikipedia.org/wiki/Minimum_spanning_tree) of a graph. 

It was originally developed by Fabien Jacques and Jonathan Narboni from [Bordeaux University](https://www.u-bordeaux.com/) 
for a second year master's course on distributed algorithm taught by [Cyril Gavoille](http://dept-info.labri.fr/~gavoille/).
It has then been refactored by Rémi Laplace and Jonathan Narboni from the [LaBRI](https://www.labri.fr).

[JBotSim][jbotsim-web] is a simulation library for distributed algorithms in dynamic networks.

The GHS algorithm builds up on merging step by step partial MSTs (called fragments) of the graph.
It increments from fragments containing only one node, to a single fragment spanning the whole graph (for more details
on our implementation, see [here](#detailed-operation)). 
At any step of the algorithm, the fragments are colored with a unique color (with respect to their ID) which makes the 
algorithm easier to follow.

## Running the GHS algorithm with JBotSim

### How to run it

The project is designed to work well with [IntelliJ](#https://www.jetbrains.com/idea/).
However, to run the application, it suffices to run the following command in a console, at the root of the project: 

```bash
./gradlew run --args="src/main/resources/gabriel_100.dot -cs 100 -d -s result.dot"
```


For further information on the available options, check the [dedicated section](#parameters).


### Color codes and states

The color of the transparent disk around each node denotes the color of the fragment which the node belongs to.

The color of the node denotes its current state:

* White: `INIT`: 

  Initial state of the node.

* Dark Red: `AWAIT_PULSE`: 

  The node is waiting for the `PULSE` message, which will come from the root.
  The node is in this state at the beginning of each phase, so it is either the first phase (just after `INIT`),
  or after the end of the previous phase.
  
* Cyan: `AWAIT_ACK_FRAGS_AND_OUT`: 

  The node has already sent the `FRAG` message to its neighbors and is waiting for their replies (`ACK_FRAG`), and
  for the `OUT` message from each of its children

* Dark Cyan: `AWAIT_CHOSEN_ROOT`: 

  The node has sent its `OUT` message to its father. It is waiting for the message which informs that the current
  root of the fragment has chosen the new root of the fragment (`CHOSEN_ROOT`).
  
* Pink: `AWAIT_NEW_MESSAGE`: 

  The node knows the new root in the former fragment. It is waiting for the message which will inform it of the
  the new fragment ID (`NEW` message). 

* Light Red: `AWAIT_ECHOS_AND_ACK_MERGES`: 

  The node has received the `NEW` message. The fragment to which it belongs has thus already merged.
  Before being able to send its `ECHO` message (terminating this phase) to its father, the node waits for:
  * the `ACK_MERGE` message from its neighbors in late fragments (meaning that this neighbor won't send a
  `MERGE` message during this phase);
  
  * the `ECHO` message from each of its children.

  Note that receiving a `MERGE` message in this state, will trigger a `NEW` message (not changing the state of the
  node) to inform the late fragment of the new fragment ID.  
  		 
  		 
* Black: `FINISHED`: 

  The node has received a `STOP` message, informing it of the end of the algorithm. This end has been detected
   by the root of the final fragment.
   
For further information on the messages, and the detailed operation of the algorithm, check 
[the dedicated section](#detailed-operation)

### Parameters



Here is the list of available options:

`-log`: log all messages (beware: it really slows down the computing).

`-v X`: verbose mode write in console every `X` messages (useful to check if the algorithm is still running or is
 blocked).

`-d`: display the topology (beware: computing without displaying the topology can cause failure).

`-s filename.dot`: save the result of the MST in dot format (edges of the MST will be in magenta).

`-p filename.csv`: add a line n,m,nbMess,nbTic to filename.csv where n is the number of vertices, m the number of 
edges, nbMess the number of messages, and nbTic the number of clock tics.

`-cs X`: change the clockspeed to `X` ms.

`-si`: shuffle the ids of the nodes before running the algorithm.

## Details on the GHS algorithm and our implementation

## Principle and complexity

The [GHS (Gallager, Humblet and Spira)][GHS-wiki] algorithm is a distributed algorithm which builds a minimum spanning 
tree (MST) on a connected graph with weighted edges. 
The principle of the algorithm is to build the MST by merging partial MSTs (called fragments) at each step. 
This version of the GHS algorithm uses an async model, and is synchronised using an 
[alpha synchronizer](https://en.wikipedia.org/wiki/Synchronizer_%28algorithm%29).
It has a time complexity of *O(n log(n))*, and a message complexity of *O(m log(n))*, where *n* is the number of vertices, 
and *m* is the number of edges of the graph.

## Detailed operation

At the beginning of the algorithm, every node is its own **fragment**: the ID of the fragment is the ID of the node. 

At each **phase** of the algorithm:
1. Each fragment (which is a rooted tree representing a partial MST of the graph) searches for the link to another 
   fragment with the smallest weight. 
   This unique link is called the fragment's **outlink**, and its weight corresponds to the Euclidian distance between 
   its nodes.
2. Then each fragment merges with another fragment with respect to its outlink. 
   The new fragment is formed from the former fragments plus the outlinks.
   When the algorithm ends, a unique fragment remains. 
   The links of this fragment represent the minimum spanning tree of the graph.


We have implemented this with a set of messages.
We explain how thereafter.

At each phase:
 
1. The root of each fragment sends a message `PULSE` to its children in the fragment.
   Its children then forward it to their children in the fragment. 
   This message is thus broadcasted to all the nodes of the fragment.

2. When a node receives a `PULSE` message (or the node is the current root of the fragment), it sends a `FRAG` message to 
   all of its neighbors. 
   When one of its neighbors receives the `FRAG` message it replies with an `ACK_FRAG` message informing the sender of 
   its fragment ID. 
   When a node has received every `ACK_FRAG` message from its neighbors, it knows the possible link to another fragment 
   with the smallest weight: its outlink.

   * If no outlink has been found, it means that there is only one fragment left, and so that the algorithm has terminated. 
   The MST of the graph is represented by this fragment, and the final root broadcasts a `STOP` message to inform the 
   other nodes that the algorithm is finished.

3. Each node of the tree needs to inform its father of the outlink of its whole subtree.
   This is done by sending an `OUT` message to its father. 
   This message contains its subtree's outlink. 
   This is ready when both:
   * it has received the `ACK_FRAG` message from all of its neighbors (out of the fragment);
   * it has received the `OUT` message from all of its children (in the fragment).

4. When the root knows the best outlink of its subtree (thus of the fragment), the node in the fragment attached to this 
   link can potentially become the new root of the fragment in the next phase. 
   The current root broadcasts a `CHOSEN_ROOT` message to inform the nodes of the fragment that a new root has been chosen. 
   When receiving this message, a node changes its father if needed (*i.e.* reversing the path to the root).

5. Upon receiving the `CHOSEN_ROOT` message, the new root sends a `MERGE` message to the other end of the outlink,  
   informing the other fragment of an upcoming merge.    
   *These `MERGE` messages can form a "chain" of merge requests between several fragments, but it can not form a cycle. 
   Therefore, for each of these chains, there exists two fragments with the same outlink chosen.*

6. This specific outlink will be the seed of the merge operation. 
   The node of this outlink belonging to the fragment with the smallest ID (which will become the ID of the resulting 
   fragment) will be finally chosen as new root of the resulting fragment. 
   This new root broadcasts a `NEW` message in the resulting fragment (possibly containing several former fragments 
   chained together) informing the nodes of their new fragment ID. 

7. When a node receives a `NEW` message, it changes its fragment ID and its father in the fragment (if needed). 
   It then informs all its neighbors that it has accomplished the merge operation by sending `ACK_MERGE` messages  
   (it is the alpha synchronizer). 
   When a node has received all the `ACK_MERGE` messages from its neighbors (meaning that all its neighbors have 
   finished the merge operation):
   * if it is a leaf, it sends a`ECHO` message to its father in the fragment;
   * if it is not a leaf, it waits for all the `ECHO` messages from its children before sending an `ECHO` message 
   to its father.

8. When the new root of the merged fragment has received all the `ACK_MERGE` messages and the `ECHO` messages, a new 
   phase can start.
   The root thus broadcasts a `PULSE` message to all the nodes in the new fragment (this is actually step `1`).


 
## License

(C) Copyright 2019, by [the jbotsim-ghs contributors](CONTRIBUTORS.md). All rights reserved.


jbotsim-ghs is published under license [LGPL 3.0 or later][lgpl3]. 


A copy of the license should be available in the product.
The repository also contains a copy of the [GPL](COPYING) and a copy of the [LGPL](COPYING.LESSER).
You can also access copies of the license at <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: LGPL-3.0-or-later



[jbotsim-web]: https://jbotsim.io/
[GHS-wiki]: https://en.wikipedia.org/wiki/Distributed_minimum_spanning_tree#GHS_algorithm 
[lgpl3]: http://www.gnu.org/licenses/lgpl-3.0.html
