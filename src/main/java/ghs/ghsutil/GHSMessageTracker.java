/*
 * Copyright 2019, the jbotsim-ghs contributors
 *
 *
 * This file is part of jbotsim-ghs.
 *
 * jbotsim-ghs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-ghs is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-ghs.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ghs.ghsutil;

import io.jbotsim.core.Node;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class GHSMessageTracker {

    private Map<FRAGEntry, Node> storeFRAGs = new ConcurrentHashMap<>();
    private Map<Integer, Integer> counterACK_MERGEs = new ConcurrentHashMap<>();
    private Set<MERGEEntry> storeMERGEs = new HashSet<>();


    private int counterACK_FRAG;
    private int counterOUT;
    private int counterECHO;

    public void clear() {
        storeFRAGs.clear();
        counterACK_MERGEs.clear();
        storeMERGEs.clear();
        resetACK_FRAG();
        resetOUT();
        resetECHO();
    }

    // region ECHO

    public void resetECHO() {
        counterECHO = 0;
    }

    public void incrementECHOs() {
        counterECHO++;
    }

    public int getECHOsValue() {
        return counterECHO;
    }
    // endregion

    // region OUT

    public void resetOUT() {
        counterOUT = 0;
    }

    public void incrementOUTs() {
        counterOUT++;
    }

    public int getOUTsValue() {
        return counterOUT;
    }
    // endregion
    // region ACK_FRAG

    public void resetACK_FRAG() {
        counterACK_FRAG = 0;
    }

    public void incrementACK_FRAGs() {
        counterACK_FRAG++;
    }

    public int getACK_FRAGsValue() {
        return counterACK_FRAG;
    }
    // endregion
    // region MERGE
    public boolean hasMERGEs() {
        return !storeMERGEs.isEmpty();
    }
    public void addMERGE(MERGEEntry mergeEntry) {
        storeMERGEs.add(mergeEntry);
    }

    public void clearMERGEs() {
        storeMERGEs.clear();
    }

    public Set<MERGEEntry> getMERGEsEntries() {
        return storeMERGEs;
    }

    public static class MERGEEntry {
        public Node node;
        public int fragNumber;

        public MERGEEntry(Node node, int fragNumber) {
            this.node = node;
            this.fragNumber = fragNumber;
        }
    }
    // endregion

    // region ACK_MERGE
    public void incrementACK_MERGEs(int phaseNumber) {
        if(!counterACK_MERGEs.containsKey(phaseNumber))
            counterACK_MERGEs.put(phaseNumber, 1);
        else
            counterACK_MERGEs.put(phaseNumber, counterACK_MERGEs.get(phaseNumber)+1);
    }

    public int getACK_MERGEsValue(int phaseNumber) {
        return counterACK_MERGEs.getOrDefault(phaseNumber, 0);
    }
    // endregion

    // region FRAGs
    public boolean hasFRAGs() {
        return !storeFRAGs.isEmpty();
    }

    public Set<FRAGEntry> getFRAGsEntries() {
        return storeFRAGs.keySet();
    }

    public Node getFRAGSender(FRAGEntry entry) {
        return storeFRAGs.get(entry);
    }

    public void removeFRAG(FRAGEntry entry) {
        storeFRAGs.remove(entry);
    }

    public void putFRAG(FRAGEntry fragEntry, Node sender) {
        storeFRAGs.put(fragEntry, sender);
    }



    public static class FRAGEntry {
        public int ID;
        public int phaseNumber;

        public FRAGEntry(int ID, int phaseNumber) {
            this.ID = ID;
            this.phaseNumber = phaseNumber;
        }
    }
    // endregion
}
